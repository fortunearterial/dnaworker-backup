using Abp.AspNetCore.Mvc.ViewComponents;

namespace DNAWorker.Web.Public.Views
{
    public abstract class DNAWorkerViewComponent : AbpViewComponent
    {
        protected DNAWorkerViewComponent()
        {
            LocalizationSourceName = DNAWorkerConsts.LocalizationSourceName;
        }
    }
}
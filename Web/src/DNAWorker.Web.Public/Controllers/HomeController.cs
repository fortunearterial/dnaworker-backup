using Microsoft.AspNetCore.Mvc;
using DNAWorker.Web.Controllers;

namespace DNAWorker.Web.Public.Controllers
{
    public class HomeController : DNAWorkerControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
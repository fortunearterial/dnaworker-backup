namespace DNAWorker
{
    public class DNAWorkerConsts
    {
        public const string LocalizationSourceName = "DNAWorker";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const int PaymentCacheDurationInMinutes = 30;
    }
}
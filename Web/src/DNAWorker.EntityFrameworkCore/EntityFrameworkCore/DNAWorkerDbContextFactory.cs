using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using DNAWorker.Configuration;
using DNAWorker.Web;

namespace DNAWorker.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class DNAWorkerDbContextFactory : IDesignTimeDbContextFactory<DNAWorkerDbContext>
    {
        public DNAWorkerDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DNAWorkerDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            DNAWorkerDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DNAWorkerConsts.ConnectionStringName));

            return new DNAWorkerDbContext(builder.Options);
        }
    }
}
using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace DNAWorker.EntityFrameworkCore
{
    public static class DNAWorkerDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DNAWorkerDbContext> builder, string connectionString)
        {
            //builder.UseSqlServer(connectionString);
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DNAWorkerDbContext> builder, DbConnection connection)
        {
            //builder.UseSqlServer(connection);
            builder.UseMySql(connection);
        }
    }
}
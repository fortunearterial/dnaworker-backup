using System.Collections.Generic;
using MvvmHelpers;
using DNAWorker.Models.NavigationMenu;

namespace DNAWorker.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}
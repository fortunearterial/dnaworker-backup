using System.Threading.Tasks;
using DNAWorker.Views;
using Xamarin.Forms;

namespace DNAWorker.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}

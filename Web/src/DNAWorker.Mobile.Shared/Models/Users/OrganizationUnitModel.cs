using Abp.AutoMapper;
using DNAWorker.Organizations.Dto;

namespace DNAWorker.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}
using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DNAWorker
{
    [DependsOn(typeof(DNAWorkerClientModule), typeof(AbpAutoMapperModule))]
    public class DNAWorkerXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DNAWorkerXamarinSharedModule).GetAssembly());
        }
    }
}
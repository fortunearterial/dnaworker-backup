using Abp.AspNetZeroCore;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.MicroKernel.Registration;
using Microsoft.Extensions.Configuration;
using DNAWorker.Configuration;
using DNAWorker.EntityFrameworkCore;
using DNAWorker.Migrator.DependencyInjection;

namespace DNAWorker.Migrator
{
    [DependsOn(typeof(DNAWorkerEntityFrameworkCoreModule))]
    public class DNAWorkerMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public DNAWorkerMigratorModule(DNAWorkerEntityFrameworkCoreModule DNAWorkerEntityFrameworkCoreModule)
        {
            DNAWorkerEntityFrameworkCoreModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(DNAWorkerMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                DNAWorkerConsts.ConnectionStringName
                );
            Configuration.Modules.AspNetZero().LicenseCode = _appConfiguration["AbpZeroLicenseCode"];

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(typeof(IEventBus), () =>
            {
                IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                );
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DNAWorkerMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
using System.Collections.Generic;
using DNAWorker.Authorization.Users.Dto;
using DNAWorker.Dto;

namespace DNAWorker.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}
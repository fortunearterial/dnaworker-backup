using System.Threading.Tasks;
using Abp.Application.Services;
using DNAWorker.Install.Dto;

namespace DNAWorker.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}
namespace DNAWorker.Install.Dto
{
    public class CheckDatabaseOutput
    {
        public bool IsDatabaseExist { get; set; }
    }
}

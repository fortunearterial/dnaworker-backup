using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DNAWorker.MultiTenancy.Accounting.Dto;

namespace DNAWorker.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}

using System.Threading.Tasks;
using Abp.Application.Services;

namespace DNAWorker.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task UpgradeTenantToEquivalentEdition(int upgradeEditionId);
    }
}

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DNAWorker.MultiTenancy.HostDashboard.Dto;

namespace DNAWorker.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}
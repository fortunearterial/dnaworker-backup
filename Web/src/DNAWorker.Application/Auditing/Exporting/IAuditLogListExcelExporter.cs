using System.Collections.Generic;
using DNAWorker.Auditing.Dto;
using DNAWorker.Dto;

namespace DNAWorker.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}

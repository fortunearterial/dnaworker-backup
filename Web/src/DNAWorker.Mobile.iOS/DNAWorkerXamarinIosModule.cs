using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DNAWorker
{
    [DependsOn(typeof(DNAWorkerXamarinSharedModule))]
    public class DNAWorkerXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DNAWorkerXamarinIosModule).GetAssembly());
        }
    }
}
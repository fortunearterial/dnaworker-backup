using DNAWorker.MultiTenancy.Accounting.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Accounting
{
    public class InvoiceViewModel
    {
        public InvoiceDto Invoice { get; set; }
    }
}

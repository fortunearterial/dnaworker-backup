using System.Collections.Generic;
using DNAWorker.Authorization.Users.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Users
{
    public class UserLoginAttemptModalViewModel
    {
        public List<UserLoginAttemptDto> LoginAttempts { get; set; }
    }
}
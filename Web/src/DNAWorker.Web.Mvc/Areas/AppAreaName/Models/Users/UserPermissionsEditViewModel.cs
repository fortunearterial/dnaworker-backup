using Abp.AutoMapper;
using DNAWorker.Authorization.Users;
using DNAWorker.Authorization.Users.Dto;
using DNAWorker.Web.Areas.AppAreaName.Models.Common;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; private set; }

        public UserPermissionsEditViewModel(GetUserPermissionsForEditOutput output, User user)
        {
            User = user;
            output.MapTo(this);
        }
    }
}
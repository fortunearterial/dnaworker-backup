using System.Collections.Generic;
using Abp.Application.Services.Dto;
using DNAWorker.Security;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Users
{
    public class UsersViewModel
    {
        public string FilterText { get; set; }

        public List<ComboboxItemDto> Permissions { get; set; }

        public List<ComboboxItemDto> Roles { get; set; }
    }
}
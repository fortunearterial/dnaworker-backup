using DNAWorker.Sessions.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Editions
{
    public class SubscriptionDashboardViewModel
    {
        public GetCurrentLoginInformationsOutput LoginInformations { get; set; }
    }
}

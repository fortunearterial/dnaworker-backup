using System.Collections.Generic;
using DNAWorker.Editions.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Tenants
{
    public class TenantIndexViewModel
    {
        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }
    }
}
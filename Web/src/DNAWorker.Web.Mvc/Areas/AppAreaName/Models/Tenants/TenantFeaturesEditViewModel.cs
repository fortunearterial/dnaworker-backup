using Abp.AutoMapper;
using DNAWorker.MultiTenancy;
using DNAWorker.MultiTenancy.Dto;
using DNAWorker.Web.Areas.AppAreaName.Models.Common;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }

        public TenantFeaturesEditViewModel(Tenant tenant, GetTenantFeaturesEditOutput output)
        {
            Tenant = tenant;
            output.MapTo(this);
        }
    }
}
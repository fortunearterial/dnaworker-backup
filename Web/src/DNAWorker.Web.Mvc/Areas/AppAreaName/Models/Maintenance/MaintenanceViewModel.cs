using System.Collections.Generic;
using DNAWorker.Caching.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using DNAWorker.Configuration.Host.Dto;
using DNAWorker.Editions.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.HostSettings
{
    public class HostSettingsViewModel
    {
        public HostSettingsEditDto Settings { get; set; }

        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }

        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}
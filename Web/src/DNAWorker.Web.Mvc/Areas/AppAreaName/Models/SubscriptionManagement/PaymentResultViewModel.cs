using Abp.AutoMapper;
using DNAWorker.Editions;
using DNAWorker.MultiTenancy.Payments.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.SubscriptionManagement
{
    [AutoMapTo(typeof(ExecutePaymentDto))]
    public class PaymentResultViewModel : SubscriptionPaymentDto
    {
        public EditionPaymentType EditionPaymentType { get; set; }
    }
}
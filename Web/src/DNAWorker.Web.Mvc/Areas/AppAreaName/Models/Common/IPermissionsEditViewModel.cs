using System.Collections.Generic;
using DNAWorker.Authorization.Permissions.Dto;

namespace DNAWorker.Web.Areas.AppAreaName.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}
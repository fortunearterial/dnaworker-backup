using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using DNAWorker.Authorization;
using DNAWorker.Web.Controllers;

namespace DNAWorker.Web.Areas.AppAreaName.Controllers
{
    [Area("AppAreaName")]
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class DashboardController : DNAWorkerControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
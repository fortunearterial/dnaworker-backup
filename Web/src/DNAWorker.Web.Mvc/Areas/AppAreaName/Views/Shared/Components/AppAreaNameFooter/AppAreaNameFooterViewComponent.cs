using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DNAWorker.Web.Areas.AppAreaName.Models.Layout;
using DNAWorker.Web.Session;
using DNAWorker.Web.Views;

namespace DNAWorker.Web.Areas.AppAreaName.Views.Shared.Components.AppAreaNameFooter
{
    public class AppAreaNameFooterViewComponent : DNAWorkerViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppAreaNameFooterViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var footerModel = new FooterViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(footerModel);
        }
    }
}

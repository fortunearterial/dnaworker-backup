using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DNAWorker.Web.Session;

namespace DNAWorker.Web.Views.Shared.Components.AccountLogo
{
    public class AccountLogoViewComponent : DNAWorkerViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AccountLogoViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var loginInfo = await _sessionCache.GetCurrentLoginInformationsAsync();
            return View(new AccountLogoViewModel(loginInfo));
        }
    }
}

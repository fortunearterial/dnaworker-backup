using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace DNAWorker.Web.Views
{
    public abstract class DNAWorkerRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected DNAWorkerRazorPage()
        {
            LocalizationSourceName = DNAWorkerConsts.LocalizationSourceName;
        }
    }
}

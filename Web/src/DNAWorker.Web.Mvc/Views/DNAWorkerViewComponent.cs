using Abp.AspNetCore.Mvc.ViewComponents;

namespace DNAWorker.Web.Views
{
    public abstract class DNAWorkerViewComponent : AbpViewComponent
    {
        protected DNAWorkerViewComponent()
        {
            LocalizationSourceName = DNAWorkerConsts.LocalizationSourceName;
        }
    }
}
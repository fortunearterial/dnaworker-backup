using System.Collections.Generic;
using Abp.Localization;
using DNAWorker.Install.Dto;

namespace DNAWorker.Web.Models.Install
{
    public class InstallViewModel
    {
        public List<ApplicationLanguage> Languages { get; set; }

        public AppSettingsJsonDto AppSettingsJson { get; set; }
    }
}

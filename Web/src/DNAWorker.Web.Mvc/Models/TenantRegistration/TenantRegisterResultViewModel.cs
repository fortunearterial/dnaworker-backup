using Abp.AutoMapper;
using DNAWorker.MultiTenancy.Dto;

namespace DNAWorker.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(RegisterTenantOutput))]
    public class TenantRegisterResultViewModel : RegisterTenantOutput
    {
        public string TenantLoginAddress { get; set; }
    }
}
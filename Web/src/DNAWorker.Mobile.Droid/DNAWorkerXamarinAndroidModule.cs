using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DNAWorker
{
    [DependsOn(typeof(DNAWorkerXamarinSharedModule))]
    public class DNAWorkerXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DNAWorkerXamarinAndroidModule).GetAssembly());
        }
    }
}
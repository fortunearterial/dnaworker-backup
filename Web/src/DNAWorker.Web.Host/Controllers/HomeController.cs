using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;

namespace DNAWorker.Web.Controllers
{
    public class HomeController : DNAWorkerControllerBase
    {
        [DisableAuditing]
        public IActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}

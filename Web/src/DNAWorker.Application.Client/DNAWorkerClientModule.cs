using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DNAWorker
{
    public class DNAWorkerClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DNAWorkerClientModule).GetAssembly());
        }
    }
}

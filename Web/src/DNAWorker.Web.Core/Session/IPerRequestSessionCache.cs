using System.Threading.Tasks;
using DNAWorker.Sessions.Dto;

namespace DNAWorker.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}

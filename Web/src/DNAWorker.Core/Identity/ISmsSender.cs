using System.Threading.Tasks;

namespace DNAWorker.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}
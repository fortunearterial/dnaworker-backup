using Abp.Domain.Services;

namespace DNAWorker
{
    public abstract class DNAWorkerDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected DNAWorkerDomainServiceBase()
        {
            LocalizationSourceName = DNAWorkerConsts.LocalizationSourceName;
        }
    }
}

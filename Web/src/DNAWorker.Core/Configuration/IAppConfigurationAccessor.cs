using Microsoft.Extensions.Configuration;

namespace DNAWorker.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}

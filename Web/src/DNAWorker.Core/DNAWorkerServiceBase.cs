using Abp;

namespace DNAWorker
{
    /// <summary>
    /// This class can be used as a base class for services in this application.
    /// It has some useful objects property-injected and has some basic methods most of services may need to.
    /// It's suitable for non domain nor application service classes.
    /// For domain services inherit <see cref="DNAWorkerDomainServiceBase"/>.
    /// For application services inherit DNAWorkerAppServiceBase.
    /// </summary>
    public abstract class DNAWorkerServiceBase : AbpServiceBase
    {
        protected DNAWorkerServiceBase()
        {
            LocalizationSourceName = DNAWorkerConsts.LocalizationSourceName;
        }
    }
}
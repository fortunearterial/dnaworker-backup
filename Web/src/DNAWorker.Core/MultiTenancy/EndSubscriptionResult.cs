namespace DNAWorker.MultiTenancy
{
    public enum EndSubscriptionResult
    {
        TenantSetInActive,
        AssignedToAnotherEdition
    }
}
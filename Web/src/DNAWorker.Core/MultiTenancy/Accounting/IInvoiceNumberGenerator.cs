using System.Threading.Tasks;
using Abp.Dependency;

namespace DNAWorker.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}
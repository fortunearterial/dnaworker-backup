using Abp.Zero.Ldap.Authentication;
using Abp.Zero.Ldap.Configuration;
using DNAWorker.Authorization.Users;
using DNAWorker.MultiTenancy;

namespace DNAWorker.Authorization.Ldap
{
    public class AppLdapAuthenticationSource : LdapAuthenticationSource<Tenant, User>
    {
        public AppLdapAuthenticationSource(ILdapSettings settings, IAbpZeroLdapModuleConfig ldapModuleConfig)
            : base(settings, ldapModuleConfig)
        {
        }
    }
}
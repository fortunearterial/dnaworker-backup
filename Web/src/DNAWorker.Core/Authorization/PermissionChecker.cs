using Abp.Authorization;
using DNAWorker.Authorization.Roles;
using DNAWorker.Authorization.Users;

namespace DNAWorker.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}

using Abp.Notifications;
using DNAWorker.Dto;

namespace DNAWorker.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}
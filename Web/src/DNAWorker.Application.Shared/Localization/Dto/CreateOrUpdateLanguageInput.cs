using System.ComponentModel.DataAnnotations;

namespace DNAWorker.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}
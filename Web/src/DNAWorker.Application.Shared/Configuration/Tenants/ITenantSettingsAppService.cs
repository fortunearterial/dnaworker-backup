using System.Threading.Tasks;
using Abp.Application.Services;
using DNAWorker.Configuration.Tenants.Dto;

namespace DNAWorker.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}

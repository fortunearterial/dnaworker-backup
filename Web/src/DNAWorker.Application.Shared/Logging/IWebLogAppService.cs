using Abp.Application.Services;
using DNAWorker.Dto;
using DNAWorker.Logging.Dto;

namespace DNAWorker.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}

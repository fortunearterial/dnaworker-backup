using System.Threading.Tasks;
using Abp.Application.Services;
using DNAWorker.Editions.Dto;
using DNAWorker.MultiTenancy.Dto;

namespace DNAWorker.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}
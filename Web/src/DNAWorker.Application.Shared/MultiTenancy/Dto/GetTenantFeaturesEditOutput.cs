using System.Collections.Generic;
using Abp.Application.Services.Dto;
using DNAWorker.Editions.Dto;

namespace DNAWorker.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}
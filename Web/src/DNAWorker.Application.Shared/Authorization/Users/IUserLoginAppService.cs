using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DNAWorker.Authorization.Users.Dto;

namespace DNAWorker.Authorization.Users
{
    public interface IUserLoginAppService : IApplicationService
    {
        Task<ListResultDto<UserLoginAttemptDto>> GetRecentUserLoginAttempts();
    }
}

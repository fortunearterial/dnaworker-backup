using System.Collections.Generic;
using DNAWorker.Authorization.Permissions.Dto;

namespace DNAWorker.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DNAWorker.Authorization.Permissions.Dto;

namespace DNAWorker.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}

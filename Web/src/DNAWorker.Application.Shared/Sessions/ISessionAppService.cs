using System.Threading.Tasks;
using Abp.Application.Services;
using DNAWorker.Sessions.Dto;

namespace DNAWorker.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}

using System.Threading.Tasks;
using DNAWorker.Security.Recaptcha;

namespace DNAWorker.Tests.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
